package com.myclude.securitytest.domain;

import com.myclude.securitytest.dtos.MemberDto;
import lombok.*;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;

@Entity
@Table(name = "member")
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Data @Builder
public class Member {
    //
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "MEMBER_USERNAME")
    private String username;

    @Column(name = "MEMBER_LOGINID")
    private String userId;

    @Column(name = "MEMBER_PASSWORD")
    private String password;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "MEMBER_ROLE")
    private UserRole userRole;

    @Column(name = "MEMBER_SOCIALID")
    private Long socialId;

    @Column(name = "MEMBER_SOCIAL_PROFILEPIC")
    private String profileHref;

    //dto변환
    public static MemberDto fromDto(Member member) {
        MemberDto dto = new MemberDto();
        BeanUtils.copyProperties(member, dto);
        return dto;
    }

}
