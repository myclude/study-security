package com.myclude.securitytest.domain;

import lombok.Getter;

/**
 * 구현의 편의성을 위해 유저 하나당 권한은 하나가 되도록 처리 한다.
 */
@Getter
public enum UserRole {
    //
    ADMIN("ROLE_ADMIN"),
    USER("ROLE_USER");

    private String roleName;

    UserRole(String roleName) {
        this.roleName = roleName;
    }

}
