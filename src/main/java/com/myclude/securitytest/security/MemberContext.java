package com.myclude.securitytest.security;

import com.myclude.securitytest.domain.Member;
import com.myclude.securitytest.domain.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * User 클래스를 상속 받은 MemberContext 만든다.
 * 여기서는 현재 Member 클래스와 Spring 에서 제공하는 User 클래스 간의 매핑을 처리하게 되며
 * 차이점은 username, password, 외 권한을 List<SimpleGrantedAuthority>형식으로 변환하여 넘기게 된다.
 */
public class MemberContext extends User {

    public MemberContext(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public static MemberContext fromAccountModel(Member member) {
        //
        return new MemberContext(member.getUserId(), member.getPassword(), parseAuthorities(member.getUserRole()));
    }

    private static List<SimpleGrantedAuthority> parseAuthorities(UserRole role) {
        //
        return Arrays.asList(role).stream().map(r -> new SimpleGrantedAuthority(r.getRoleName())).collect(Collectors.toList());
    }
}
