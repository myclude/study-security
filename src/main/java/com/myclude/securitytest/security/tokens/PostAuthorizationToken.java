package com.myclude.securitytest.security.tokens;

import com.myclude.securitytest.security.MemberContext;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class PostAuthorizationToken extends UsernamePasswordAuthenticationToken {

    /**
     * 1. 유저 정보를 인증과정에서 처리 하는 방식
     * 2. userDetails 구현체를 사용 (구현체 사용) spring 의 user를 사용
     */
    public PostAuthorizationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }

    public static PostAuthorizationToken getTokenFromMemberContext(MemberContext context) {
        return new PostAuthorizationToken(context, context.getPassword(), context.getAuthorities());
    }

    public String getUsername() {
        //
        return (String)super.getPrincipal();
    }

    public String getUserPassword() {
        //
        return (String)super.getCredentials();
    }
}
