package com.myclude.securitytest.security.tokens;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class PreAuthorizationToken extends UsernamePasswordAuthenticationToken {

//    public PostAuthorizationToken(Object principal, Object credentials) {
//        super(principal, credentials);
//    }

    /**
     * 위의 것을 구현하려는 항목
     */
    public PreAuthorizationToken(String username, String password) {
        super(username, password);
    }

    public String getUsername() {
        //
        return (String)super.getPrincipal();
    }

    public String getUserPassword() {
        //
        return (String)super.getCredentials();
    }

}
