package com.myclude.securitytest.security.providers;

import com.myclude.securitytest.domain.Member;
import com.myclude.securitytest.repository.MemberRepository;
import com.myclude.securitytest.security.MemberContext;
import com.myclude.securitytest.security.tokens.PostAuthorizationToken;
import com.myclude.securitytest.security.tokens.PreAuthorizationToken;
import com.myclude.securitytest.service.MemberContextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.NoSuchElementException;
import java.util.Optional;

/**
 *  provider 구현 (명시적 선언)
 */
public class FromLoginAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private MemberContextService memberContextService;

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        //
        PreAuthorizationToken token = (PreAuthorizationToken) authentication;

        String username = token.getUsername();
        String password = token.getUserPassword();

        Member findMember = memberRepository.findByUserId(username).orElseThrow(() -> new NoSuchElementException("정보에 맞는 계정이 없습니다."));

        if(isCorrectPassword(password, findMember)) {
            //
            return PostAuthorizationToken.getTokenFromMemberContext(MemberContext.fromAccountModel(findMember));
        }
        throw new NoSuchElementException("인증 정보가 정확하지 않습니다.");
    }

    @Override
    /**
     * 해당 provider가 어떤 인증 객체를 support를 할 지 명시적으로 선언을 해 주어야 한다.
     * 여기서는 preAuthorizationToken으로 들어오는 요청을 모두 해당 provider에서 support 하게 된다. (여기를 거침)
     */
    public boolean supports(Class<?> authentication) {
        return PreAuthorizationToken.class.isAssignableFrom(authentication);
    }

    //matches 에서 앞자리는 원본이 와야 한다. //순서가 변경이 되면 에러가 난다.
    private boolean isCorrectPassword(String password, Member member) {
        //
        return passwordEncoder.matches(member.getPassword(), password);
    }
}
