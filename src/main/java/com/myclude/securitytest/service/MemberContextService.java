package com.myclude.securitytest.service;

import com.myclude.securitytest.domain.Member;
import com.myclude.securitytest.repository.MemberRepository;
import com.myclude.securitytest.security.MemberContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.NoSuchElementException;

/**
 * UserDetail Service를 구현
 */
@Component
public class MemberContextService implements UserDetailsService {
    //
    @Autowired
    private MemberRepository memberRepository;

    @Override
    public UserDetails loadUserByUsername(String userid) throws UsernameNotFoundException {
        Member member = memberRepository.findByUserId(userid)
                .orElseThrow(() -> new NoSuchElementException("아이디에 맞는 계정이 없습니다."));

        return getMemberContext(member);
    }

    private MemberContext getMemberContext(Member member) {
        //
        return MemberContext.fromAccountModel(member);
    }
}
