package com.myclude.securitytest.repository;

import com.myclude.securitytest.domain.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    //
    Optional<Member> findByUserId(String userId);
    Optional<Member> findBySocialId(Long socialId);
}
